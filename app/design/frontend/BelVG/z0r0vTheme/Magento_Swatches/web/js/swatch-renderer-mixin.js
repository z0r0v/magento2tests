define([
    'jquery',
    'underscore',
    'mage/template',
    'mage/smart-keyboard-handler',
    'mage/translate',
    'priceUtils',
    'jquery-ui-modules/widget',
    'jquery/jquery.parsequery',
    'mage/validation/validation'

], function ($, _, mageTemplate, keyboardHandler, $t, priceUtils) {
    'use strict';



    var catchClick = {

        _OnClick: function ($this, $widget) {

            var parent = $this.parents().parents();

            if (parent.hasClass('size')) {
                console.log("changed size: " + $this.text());
            }
            
            else if (parent.hasClass('color')) {
                console.log("changed color: " + $this.attr('option-label'));
            }

            return this._super($this, $widget);


        }
    };


    return function (targetWidget) {
        // Example how to extend a widget by mixin object
        $.widget('mage.SwatchRenderer', targetWidget, catchClick); // the widget alias should be like for the target widget

        return $.mage.SwatchRenderer; //  the widget by parent alias should be returned
    };
});