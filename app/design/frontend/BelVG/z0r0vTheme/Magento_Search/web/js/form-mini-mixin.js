define([
    'jquery',
    'jquery-ui-modules/widget',
    'jquery-ui-modules/core',
    ], function ($) {
    'use strict';

    var searchMixin = {


        setActiveState: function (isActive) {
            console.log("You in search");
            return this._super();
        }


    };

    return function (targetWidget) {
        // Example how to extend a widget by mixin object
        $.widget('mage.quickSearch', targetWidget, searchMixin); // the widget alias should be like for the target widget

        return $.mage.quickSearch; //  the widget by parent alias should be returned
    };
});