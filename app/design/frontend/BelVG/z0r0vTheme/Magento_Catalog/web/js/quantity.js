require(['jquery', 'jquery/ui'], function($){

    var data = {
        inputQty: $('#qty'),
        buttonPlus: $('.quantity-plus'),
        buttonMinus: $('.quantity-minus'),
    };

    data.inputQtyVal = data.inputQty.val();

    const methods = {
        quantityPlus:function () {
            data.inputQtyVal = ++data.inputQtyVal;
            data.inputQty.val(data.inputQtyVal);

        },

        quantityMinus:function() {
            if(data.inputQtyVal > 0) {
                data.inputQtyVal = --data.inputQtyVal;
                data.inputQty.val(data.inputQtyVal);
            }
        }

    };

    data.buttonPlus.on('click', methods.quantityPlus);
    data.buttonMinus.on('click', methods.quantityMinus);

});

